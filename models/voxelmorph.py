import torch
import monai
import torch.nn as nn
import numpy as np
from models.spatial_transformation import SpatialTransformer
from torch.distributions.normal import Normal
## Building the Voxelmorph model ##

class VoxelMorph64(nn.Module):
    '''
    Implements the entire Voxelmorph module for 64 * 64 volumes with the help of monai and pytorch.
    '''

    def __init__(self, input_shape , in_channels = 2 , out_channels = 16 ,
                 depth = 6 , kernel_size = 3 
                 ):
        '''
        Constructor for the Voxelmorph class.
        :param input_shape: The shape of the inputs, typically, Volume , Height, Width.
        :param in_channels: Number of Input channels to the UNET. (Defaults: 2).
        :param out_channels: Number of output channels of the UNET. (Default: 16).
        We add another layer after the UNET to get the flow, so try not to give the
        final output channel here.
        :param depth: The depth of the UNET, i.e. number of times it downsamples before it starts upsampling.
	    (Defaults: 6)
        :param kernel_size: Convolutional kernel size of UNET. (Defaults: 3).
        '''
        super().__init__()

        self.input_shape = input_shape
        self.spatial_dim = len(self.input_shape)

        ## Defining the Unet which returns the flow field ##

        self.unet = monai.networks.nets.RegUNet(spatial_dims=self.spatial_dim,
                                                in_channels=in_channels,
                                                num_channel_initial=in_channels,
						                        depth = depth,
                                             	out_channels=out_channels,
                                             	pooling=True,
                                             	concat_skip = True,
						                        encode_kernel_sizes = kernel_size
                                             )

        ## Defining the flow producing CNN ##

        self.flow_producer = nn.Conv3d(in_channels=out_channels,
                                       out_channels=self.spatial_dim,
                                       kernel_size=3,
                                       padding=1,
                                       )

        ## Init flow layer with small weights and zero bias ##

        self.flow_producer.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer.weight.shape))
        self.flow_producer.bias = nn.Parameter(torch.zeros(self.flow_producer.bias.shape))

        ## Setting the Spatial Transformer ##

        self.spatial_transformer = SpatialTransformer(self.input_shape)

    def forward(self, fixed_vol, moving_vol , moving_vol_seg = None):
        '''
        Takes the fixed volume and moving volume as input and gives out the flow field
        and the warped volume as output.
        Optionally takes the moving volume segmentations.
        '''

        input_vol = torch.cat([fixed_vol, moving_vol], dim=1)

        flow_field = self.flow_producer(self.unet(input_vol))

        pred_fixed_vol = self.spatial_transformer(moving_vol, flow_field)

        if moving_vol_seg is not None:
            pred_fixed_seg = self.spatial_transformer(moving_vol_seg, flow_field)
            return flow_field, pred_fixed_vol, pred_fixed_seg

        return flow_field, pred_fixed_vol


class VoxelMorph128(nn.Module):
    '''
    Implements the entire Voxelmorph module for 64 * 64 volumes with the help of monai and pytorch.
    '''

    def __init__(self, input_shape , in_channels = 2 , out_channels = 16 ,
                 depth = 6 , kernel_size = 3
                 ):
        '''
        Constructor for the Voxelmorph class.
        :param input_shape: The shape of the inputs, typically, Volume , Height, Width.
        :param in_channels: Number of Input channels to the UNET. (Defaults: 2).
        :param out_channels: Number of output channels of the UNET. (Default: 16).
        We add another layer after the UNET to get the flow, so try not to give the
        final output channel here.
        :param depth: The depth of the UNET, i.e. number of times it downsamples before it starts upsampling.
	    (Defaults: 7)
        :param kernel_size: Convolutional kernel size of UNET. (Defaults: 3).
        '''
        super().__init__()

        self.input_shape = input_shape
        self.spatial_dim = len(self.input_shape)

        ## Defining the Unet which returns the flow field ##

        self.unet = monai.networks.nets.RegUNet(spatial_dims=self.spatial_dim,
                                                in_channels=in_channels,
                                                num_channel_initial=in_channels,
						                        depth = depth,
                                             	out_channels=out_channels,
                                             	pooling=True,
                                             	concat_skip = True,
						                        encode_kernel_sizes = kernel_size
                                             )

        ## Defining the flow producing CNN ##

        self.flow_producer = nn.Conv3d(in_channels=out_channels,
                                       out_channels=self.spatial_dim,
                                       kernel_size=3,
                                       padding=1,
                                       )

        ## Init flow layer with small weights and zero bias ##

        self.flow_producer.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer.weight.shape))
        self.flow_producer.bias = nn.Parameter(torch.zeros(self.flow_producer.bias.shape))

        ## Setting the Spatial Transformer ##

        self.spatial_transformer = SpatialTransformer(self.input_shape)

    def forward(self, fixed_vol, moving_vol , moving_vol_seg = None):
        '''
        Takes the fixed volume and moving volume as input and gives out the flow field
        and the warped volume as output.
        Optionally takes the moving volume segmentations.
        '''

        input_vol = torch.cat([fixed_vol, moving_vol], dim=1)

        flow_field = self.flow_producer(self.unet(input_vol))

        pred_fixed_vol = self.spatial_transformer(moving_vol, flow_field)

        if moving_vol_seg is not None:
            pred_fixed_seg = self.spatial_transformer(moving_vol_seg, flow_field)
            return flow_field, pred_fixed_vol, pred_fixed_seg

        return flow_field, pred_fixed_vol


class VoxelMorph256(nn.Module):
    '''
    Implements the entire Voxelmorph module for 64 * 64 volumes with the help of monai and pytorch.
    '''

    def __init__(self, input_shape , in_channels = 2 , out_channels = 16 ,
                 depth = 6 , kernel_size = 3
                 ):
        '''
        Constructor for the Voxelmorph class.
        :param input_shape: The shape of the inputs, typically, Volume , Height, Width.
        :param in_channels: Number of Input channels to the UNET. (Defaults: 2).
        :param out_channels: Number of output channels of the UNET. (Default: 16).
        We add another layer after the UNET to get the flow, so try not to give the
        final output channel here.
        :param depth: The depth of the UNET, i.e. number of times it downsamples before it starts upsampling.
	    (Defaults: 6)
        :param kernel_size: Convolutional kernel size of UNET. (Defaults: 3).
        '''
        super().__init__()

        self.input_shape = input_shape
        self.spatial_dim = len(self.input_shape)

        ## Defining the Unet which returns the flow field ##

        self.unet = monai.networks.nets.RegUNet(spatial_dims=self.spatial_dim,
                                                in_channels=in_channels,
                                                num_channel_initial=in_channels,
						                        depth = depth,
                                             	out_channels=out_channels,
                                             	pooling=True,
                                             	concat_skip = True,
						                        encode_kernel_sizes = kernel_size
                                             )

        ## Defining the flow producing CNN ##

        self.flow_producer = nn.Conv3d(in_channels=out_channels,
                                       out_channels=self.spatial_dim,
                                       kernel_size=3,
                                       padding=1,
                                       )

        ## Init flow layer with small weights and zero bias ##

        self.flow_producer.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer.weight.shape))
        self.flow_producer.bias = nn.Parameter(torch.zeros(self.flow_producer.bias.shape))

        ## Setting the Spatial Transformer ##

        self.spatial_transformer = SpatialTransformer(self.input_shape)

    def forward(self, fixed_vol, moving_vol , moving_vol_seg = None):
        '''
        Takes the fixed volume and moving volume as input and gives out the flow field
        and the warped volume as output.
        Optionally takes the moving volume segmentations.
        '''

        input_vol = torch.cat([fixed_vol, moving_vol], dim=1)

        flow_field = self.flow_producer(self.unet(input_vol))

        pred_fixed_vol = self.spatial_transformer(moving_vol, flow_field)

        if moving_vol_seg is not None:
            pred_fixed_seg = self.spatial_transformer(moving_vol_seg, flow_field)
            return flow_field, pred_fixed_vol, pred_fixed_seg

        return flow_field, pred_fixed_vol


