from torchsummary import summary
import torch

def model_summary(model , input_vol_shape):
    '''
    Gives a detailed summary of the model by using torchsummary.
    :param model: Any Pytorch Model. (Here it will ofcourse be VoxelMorph3D).
    :param input_vol_shape: The shape of the input volume. Here just give the
    shape like (Height , Width , Volume).
    :return: A detailed summary of the model.
    '''

    h , w , v = input_vol_shape

    fake_fix_vol = torch.randn((1 , 1 , h , w , v))
    fake_mov_vol = torch.randn((1 , 1 , h , w , v))

    summary(model , [fake_fix_vol , fake_mov_vol])