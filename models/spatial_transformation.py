import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

## Building the Spatial Transformer module ##

## Code inspired from https://github.com/voxelmorph/voxelmorph/blob/dev/voxelmorph/torch/layers.py ##

class SpatialTransformer(nn.Module):
    """
    Implements the Spatial Transformer module.
    This is needed because the affine_grid of Pytorch only works for 3 * 2 affine matrix.
    """

    def __init__(self, vol_shape, mode='bilinear'):
        '''
        Parameters:
        size: Shape of the volume. Used to create the coordinate of the volume.
        mode: Value input at the specific point of the transformed grid requires interpolation. This sets
        the mode of the interpolation. (Defaults: 'bilinear')
        '''
        super().__init__()

        ## Setting the interpolation mode ##

        self.mode = mode

        ## Making the grid ##
        vectors = [torch.arange(0, v) for v in vol_shape]
        grids = torch.meshgrid(vectors)
        grid = torch.stack(grids)
        grid = torch.unsqueeze(grid, 0)
        grid = grid.type(torch.FloatTensor)

        self.register_buffer('grid', grid)

    def forward(self, vol, flow):
        '''
        Parameters:
        vol: The fixed volume that is transformed.
        flow: The flow field or the transformation field.
        '''

        ## Getting the new locations of the volume to be transformed ##

        new_locs = self.grid + flow
        shape = flow.shape[2:]

        ## Normalizing the values of the grid to lie between (-1 , 1) ##

        for i in range(len(shape)):
            new_locs[:, i, ...] = 2 * (new_locs[:, i, ...] / (shape[i] - 1) - 0.5)

        ## Moving the channels axis to the last position ##

        new_locs = new_locs.permute(0, 2, 3, 4, 1)
        new_locs = new_locs[..., [2, 1, 0]]

        return F.grid_sample(vol, new_locs, align_corners=True, mode=self.mode)