import torch
import monai
import torch.nn as nn
import numpy as np
from models.spatial_transformation import SpatialTransformer
from torch.distributions.normal import Normal
## Building the Voxelmorph model ##

class RingMorph64(nn.Module):
    '''
    Implements the entire Ringmorph module for 64 * 64 volumes with the help of monai and pytorch.
    '''

    def __init__(self, input_shape , in_channels = 2 , out_channels = 16 ,
                 filters = (16, 32, 64, 128, 256, 512) , kernel_size = 3 ,
                 strides = (2, 2, 2, 2, 2) , num_res_units=2, act='PRELU',
                 norm='BATCH', dropout=0.2):
        '''
        Constructor for the Voxelmorph class.
        :param input_shape: The shape of the inputs, typically, Volume , Height, Width.
        :param in_channels: Number of Input channels to the UNET. (Defaults: 2).
        :param out_channels: Number of output channels of the UNET. (Default: 16).
        We add another layer after the UNET to get the flow, so try not to give the
        final output channel here.
        :param filters: The number of filters for the 3D UNET analysis block.
        The last filter is of the Bridge Block.
        (Default: (16, 32, 64, 128, 256, 512)).
        :param kernel_size: Convolutional kernel size of UNET. (Defaults: 3).
        :param strides: Strides after each of the synthesis and analysis blocks.
        (Defaults: (2, 2, 2, 2, 2)).
        :param num_res_units: Number of residual units taken in the layer wise Convolution of
        the UNET. (Defaults: 2).
        :param act: The Activation function following the convolutional blocks.
        (Defaults: 'PRELU').
        :param norm: Decides which normalization to use to reduce regularization. (Defaults: 'BATCH').
        :param dropout: Decides the percentage of Dropout to use. (Defaults: 0.2).
        '''
        super().__init__()

        self.input_shape = input_shape
        self.spatial_dim = len(self.input_shape)

        ## Defining the Unet which returns the flow field ##

        self.unet = monai.networks.nets.UNet(spatial_dims=self.spatial_dim,
                                             in_channels=in_channels,
                                             out_channels=out_channels,
                                             channels=filters,
                                             kernel_size=kernel_size,
                                             strides=strides,
                                             num_res_units=num_res_units,
                                             act=act,
                                             norm=norm,
                                             dropout=dropout,
                                             )

        ## Defining the flow producing CNN ##

        self.flow_producer_1 = nn.Conv3d(in_channels=out_channels,
                                       out_channels=self.spatial_dim,
                                       kernel_size=3,
                                       padding=1
                                       )

        ## Init flow layer with small weights and zero bias ##

        self.flow_producer_1.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer_1.weight.shape))
        self.flow_producer_1.bias = nn.Parameter(torch.zeros(self.flow_producer_1.bias.shape))

        self.flow_producer_2 = nn.Conv3d(in_channels=out_channels,
                                         out_channels=self.spatial_dim,
                                         kernel_size=3,
                                         padding=1
                                         )

        self.flow_producer_2.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer_2.weight.shape))
        self.flow_producer_2.bias = nn.Parameter(torch.zeros(self.flow_producer_2.bias.shape))

        ## Setting the Spatial Transformer ##

        self.spatial_transformer = SpatialTransformer(self.input_shape)

    def forward(self, fixed_vol, moving_vol, fixed_vol_seg=None):
        '''
        Takes the fixed volume and moving volume as input and gives out the flow field
        and the warped volume as output.
        Optionally takes the moving volume segmentations.
        '''

        input_vol_1 = torch.cat([fixed_vol, moving_vol], dim=1)

        flow_field_1 = self.flow_producer_1(self.unet(input_vol_1))

        pred_fixed_vol = self.spatial_transformer(moving_vol, flow_field_1)

        input_vol_2 = torch.cat([moving_vol, fixed_vol], dim=1)

        flow_field_2 = self.flow_producer_2(self.unet(input_vol_2))

        pred_moving_vol = self.spatial_transformer(fixed_vol, flow_field_2)
        if fixed_vol_seg is not None:
            pred_mov_seg = self.spatial_transformer(fixed_vol_seg, flow_field_2)
            return flow_field_2, pred_fixed_vol, pred_mov_seg

        return flow_field_1, pred_fixed_vol , flow_field_2 , pred_moving_vol


class RingMorph128(nn.Module):
    '''
    Implements the entire Ringmorph module for 64 * 64 volumes with the help of monai and pytorch.
    '''

    def __init__(self, input_shape , in_channels = 2 , out_channels = 16 ,
                 filters = (16, 32, 64, 128, 256, 512 , 1024) , kernel_size = 3 ,
                 strides = (2, 2, 2, 2, 2 , 2) , num_res_units=2, act='PRELU',
                 norm='BATCH', dropout=0.2):
        '''
        Constructor for the Voxelmorph class.
        :param input_shape: The shape of the inputs, typically, Volume , Height, Width.
        :param in_channels: Number of Input channels to the UNET. (Defaults: 2).
        :param out_channels: Number of output channels of the UNET. (Default: 16).
        We add another layer after the UNET to get the flow, so try not to give the
        final output channel here.
        :param filters: The number of filters for the 3D UNET analysis block.
        The last filter is of the Bridge Block.
        (Default: (16, 32, 64, 128, 256, 512)).
        :param kernel_size: Convolutional kernel size of UNET. (Defaults: 3).
        :param strides: Strides after each of the synthesis and analysis blocks.
        (Defaults: (2, 2, 2, 2, 2)).
        :param num_res_units: Number of residual units taken in the layer wise Convolution of
        the UNET. (Defaults: 2).
        :param act: The Activation function following the convolutional blocks.
        (Defaults: 'PRELU').
        :param norm: Decides which normalization to use to reduce regularization. (Defaults: 'BATCH').
        :param dropout: Decides the percentage of Dropout to use. (Defaults: 0.2).
        '''
        super().__init__()

        self.input_shape = input_shape
        self.spatial_dim = len(self.input_shape)

        ## Defining the Unet which returns the flow field ##

        self.unet = monai.networks.nets.UNet(spatial_dims=self.spatial_dim,
                                             in_channels=in_channels,
                                             out_channels=out_channels,
                                             channels=filters,
                                             kernel_size=kernel_size,
                                             strides=strides,
                                             num_res_units=num_res_units,
                                             act=act,
                                             norm=norm,
                                             dropout=dropout,
                                             )

        ## Defining the flow producing CNN ##

        self.flow_producer_1 = nn.Conv3d(in_channels=out_channels,
                                         out_channels=self.spatial_dim,
                                         kernel_size=3,
                                         padding=1
                                         )

        ## Init flow layer with small weights and zero bias ##

        self.flow_producer_1.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer_1.weight.shape))
        self.flow_producer_1.bias = nn.Parameter(torch.zeros(self.flow_producer_1.bias.shape))

        self.flow_producer_2 = nn.Conv3d(in_channels=out_channels,
                                         out_channels=self.spatial_dim,
                                         kernel_size=3,
                                         padding=1
                                         )

        self.flow_producer_2.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer_2.weight.shape))
        self.flow_producer_2.bias = nn.Parameter(torch.zeros(self.flow_producer_2.bias.shape))

        ## Setting the Spatial Transformer ##

        self.spatial_transformer = SpatialTransformer(self.input_shape)

    def forward(self, fixed_vol, moving_vol, fixed_vol_seg=None):
        '''
        Takes the fixed volume and moving volume as input and gives out the flow field
        and the warped volume as output.
        Optionally takes the moving volume segmentations.
        '''

        input_vol_1 = torch.cat([fixed_vol, moving_vol], dim=1)

        flow_field_1 = self.flow_producer_1(self.unet(input_vol_1))

        pred_fixed_vol = self.spatial_transformer(moving_vol, flow_field_1)

        input_vol_2 = torch.cat([moving_vol, fixed_vol], dim=1)

        flow_field_2 = self.flow_producer_2(self.unet(input_vol_2))

        pred_moving_vol = self.spatial_transformer(fixed_vol, flow_field_2)

        if fixed_vol_seg is not None:
            pred_mov_seg = self.spatial_transformer(fixed_vol_seg, flow_field_2)
            return flow_field_2, pred_fixed_vol, pred_mov_seg

        return flow_field_1, pred_fixed_vol , flow_field_2 , pred_moving_vol


class RingMorph256(nn.Module):
    '''
    Implements the entire Ringmorph module for 64 * 64 volumes with the help of monai and pytorch.
    '''

    def __init__(self, input_shape , in_channels = 2 , out_channels = 16 ,
                 filters = (16, 32, 64, 128, 256, 512 , 512 , 1024) , kernel_size = 3 ,
                 strides = (2, 2, 2, 2, 2 , 2 , 2) , num_res_units=2, act='PRELU',
                 norm='BATCH', dropout=0.2):
        '''
        Constructor for the Voxelmorph class.
        :param input_shape: The shape of the inputs, typically, Volume , Height, Width.
        :param in_channels: Number of Input channels to the UNET. (Defaults: 2).
        :param out_channels: Number of output channels of the UNET. (Default: 16).
        We add another layer after the UNET to get the flow, so try not to give the
        final output channel here.
        :param filters: The number of filters for the 3D UNET analysis block.
        The last filter is of the Bridge Block.
        (Default: (16, 32, 64, 128, 256, 512)).
        :param kernel_size: Convolutional kernel size of UNET. (Defaults: 3).
        :param strides: Strides after each of the synthesis and analysis blocks.
        (Defaults: (2, 2, 2, 2, 2)).
        :param num_res_units: Number of residual units taken in the layer wise Convolution of
        the UNET. (Defaults: 2).
        :param act: The Activation function following the convolutional blocks.
        (Defaults: 'PRELU').
        :param norm: Decides which normalization to use to reduce regularization. (Defaults: 'BATCH').
        :param dropout: Decides the percentage of Dropout to use. (Defaults: 0.2).
        '''
        super().__init__()

        self.input_shape = input_shape
        self.spatial_dim = len(self.input_shape)

        ## Defining the Unet which returns the flow field ##

        self.unet = monai.networks.nets.UNet(spatial_dims=self.spatial_dim,
                                             in_channels=in_channels,
                                             out_channels=out_channels,
                                             channels=filters,
                                             kernel_size=kernel_size,
                                             strides=strides,
                                             num_res_units=num_res_units,
                                             act=act,
                                             norm=norm,
                                             dropout=dropout,
                                             )

        ## Defining the flow producing CNN ##

        self.flow_producer_1 = nn.Conv3d(in_channels=out_channels,
                                         out_channels=self.spatial_dim,
                                         kernel_size=3,
                                         padding=1
                                         )

        ## Init flow layer with small weights and zero bias ##

        self.flow_producer_1.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer_1.weight.shape))
        self.flow_producer_1.bias = nn.Parameter(torch.zeros(self.flow_producer_1.bias.shape))

        self.flow_producer_2 = nn.Conv3d(in_channels=out_channels,
                                         out_channels=self.spatial_dim,
                                         kernel_size=3,
                                         padding=1
                                         )

        self.flow_producer_2.weight = nn.Parameter(Normal(0, 1e-5).sample(self.flow_producer_2.weight.shape))
        self.flow_producer_2.bias = nn.Parameter(torch.zeros(self.flow_producer_2.bias.shape))

        ## Setting the Spatial Transformer ##

        self.spatial_transformer = SpatialTransformer(self.input_shape)

    def forward(self, fixed_vol, moving_vol, fixed_vol_seg=None):
        '''
        Takes the fixed volume and moving volume as input and gives out the flow field
        and the warped volume as output.
        Optionally takes the moving volume segmentations.
        '''

        input_vol_1 = torch.cat([fixed_vol, moving_vol], dim=1)

        flow_field_1 = self.flow_producer_1(self.unet(input_vol_1))

        pred_fixed_vol = self.spatial_transformer(moving_vol, flow_field_1)

        input_vol_2 = torch.cat([moving_vol, fixed_vol], dim=1)

        flow_field_2 = self.flow_producer_2(self.unet(input_vol_2))

        pred_moving_vol = self.spatial_transformer(fixed_vol, flow_field_2)
        if fixed_vol_seg is not None:
            pred_mov_seg = self.spatial_transformer(fixed_vol_seg, flow_field_2)
            return flow_field_2, pred_fixed_vol, pred_mov_seg

        return flow_field_1, pred_fixed_vol , flow_field_2 , pred_moving_vol