from data_utilities.dataset import make_dataset, make_dataloader, give_dataset_insight, give_dl_insight
from models.ringmorph import RingMorph128
from losses.loss import smoothing_loss, segmentation_loss, similarity_loss, ContrastiveLoss
from utility.default_device import get_device

import torch
import monai
import numpy as np
from tqdm import tqdm
import pandas as pd

## Setting the hyperparameters ##
#SEG_REG = 10
SMOOTH_REG = 0.1
NUM_EPOCHS = 500
SEG = False

## Setting the default device ##
device = get_device()

## Setting the Dataset and the Dataloader ##
train_dataset = make_dataset('data/train_data' , spatial_size = (128 , 128 , 64) , seg=SEG , modality = 'SAME')
train_dl = make_dataloader(train_dataset)

## Setting the VoxelMorph ##
vxm = RingMorph128(input_shape=(128 , 128 , 64)).to(device)

## Setting the similarity and segmentation loss functions ##
seg_loss = segmentation_loss()
sim_loss_1 = similarity_loss(kernel_size=5)
sim_loss_2 = similarity_loss(kernel_size=5)
con_loss_1 = ContrastiveLoss(margin=1.0)
con_loss_2 = ContrastiveLoss(margin=1.0)
## Setting the optimizer ##
optim = torch.optim.Adam(vxm.parameters() , lr = 1e-3 , amsgrad = True)

total_loss_list = []
con_loss_list = []
sim_loss_list = []
smooth_loss_list = []

for epoch in range(NUM_EPOCHS):

    loop = tqdm(train_dl)

    batch_total_loss_list = []
    batch_con_loss_list = []
    batch_sim_loss_list = []
    batch_smooth_loss_list = []

    for batch in loop:

        fix = batch['fix'].to(device)
        mov = batch['mov'].to(device)

        optim.zero_grad()


        flow_field_1, pred_fixed_vol , flow_field_2 , pred_moving_vol = vxm(fix, mov)

        # else:
        #     fix_seg = batch['fix_seg'].to(device)
        #     flow_field, pred_fixed , pred_fixed_seg = vxm(fix, mov , fix_seg)


        sm_loss_1 = smoothing_loss(flow_field_1, penalty='l2')
        sm_loss_2 = smoothing_loss(flow_field_2 , penalty='l2')

        print('Pred fixed vol shape :' , pred_fixed_vol.shape)
        print('Fix vol shape :' , fix.shape)
        print('Moving vol shape :' , pred_moving_vol.shape)
        print('Mov vol shape :' , mov.shape)
        si_loss_1 = -sim_loss_1(pred_fixed_vol, fix)
        si_loss_2 = -sim_loss_2(pred_moving_vol , mov)

        cn_loss_1 = con_loss_1(pred_fixed_vol , pred_moving_vol)
        cn_loss_2 = con_loss_2(flow_field_1 , flow_field_2)

        
        total_loss =  si_loss_1 + si_loss_2 + SMOOTH_REG * (sm_loss_1 + sm_loss_2) + 2.0 * (cn_loss_1 + cn_loss_2)

        total_loss.backward()

        optim.step()

        si_loss = (si_loss_1.item() + si_loss_2.item()) / 2.0
        sm_loss = (sm_loss_1.item() + sm_loss_2.item()) / 2.0
        con_loss = (cn_loss_1.item() + cn_loss_2.item()) / 2.0
        batch_total_loss_list.append(total_loss.item())
        batch_sim_loss_list.append(si_loss)
        batch_smooth_loss_list.append(sm_loss)
        batch_con_loss_list.append(con_loss)


        loop.set_description('Epoch : {} / {}'.format(epoch + 1, NUM_EPOCHS))

        loop.set_postfix(loss=total_loss.item(), smooth_loss=sm_loss,
                             similarity_loss=si_loss, contrastive_loss=con_loss)

    total_loss_list.append(sum(batch_total_loss_list) / len(batch_total_loss_list))
    con_loss_list.append(sum(batch_con_loss_list) / len(batch_con_loss_list))
    sim_loss_list.append(sum(batch_sim_loss_list) / len(batch_sim_loss_list))
    smooth_loss_list.append(sum(batch_smooth_loss_list) / len(batch_smooth_loss_list))

    if (epoch + 1) % 100 == 0:
        torch.save(vxm.state_dict() , 'model_dir/model_{:03d}.pt'.format(epoch + 1))

loss_df = pd.DataFrame(list(zip(total_loss_list , con_loss_list,
                                    sim_loss_list , smooth_loss_list)) ,
                           columns = ['Total Loss' , 'Contrastive Loss' ,
                                      'Similarity Loss' , 'Smoothing Loss'])


loss_df.to_csv('loss.csv' , header=True , index=False)