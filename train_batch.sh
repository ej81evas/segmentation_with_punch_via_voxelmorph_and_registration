#!/bin/bash -l
#
# name the job
#SBATCH --job-name=iwb0009h_voxelmorph
# set the time limit
#SBATCH --time=20:00:00
# allocate GPU
#SBATCH --partition=a100 --gres=gpu:a100:1
# set mailing notifications
#SBATCH --mail-type=ALL
#SBATCH --mail-user=arijit.ghosh@fau.de
# set output and error place
#SBATCH -o /home/hpc/iwb0/iwb0009h/voxelmorph-segmentation/report/slurm-%j.out
#SBATCH -e /home/hpc/iwb0/iwb0009h/voxelmorph-segmentation/report/slurm-%j.err

# donot export environment variables
unset SLURM_EXPORT_ENV 

# instantiating the anaconda environment
module load python/3.8-anaconda
source activate voxelmorph_env


python test_ringmorph.py