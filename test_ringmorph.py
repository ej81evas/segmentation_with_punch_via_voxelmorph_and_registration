import torch
import numpy as np
from glob import glob
from data_utilities.dataset import make_dataset, make_dataloader , give_dataset_insight
from utility.default_device import get_device
from tqdm import tqdm
from losses.loss import smoothing_loss, segmentation_loss, similarity_loss
from utility.segment_slice_vis import visualize_seg_slice
#from utility.flow_field_vis import visualize_flow_field
from models.ringmorph import RingMorph64

## Setting the default device ##
device = get_device()

SEG = True
SPATIAL_SIZE = (64 , 64 , 64)

## Setting the model path ##
model_path = sorted(glob('model_dir' + '/*'))[-1]

## Loading the model ##
vxm = RingMorph64(SPATIAL_SIZE)
vxm.load_state_dict(torch.load(model_path))
vxm = vxm.to(device).eval()
print('Model loaded Successfully!')

## Making test dataset ##
test_dataset = make_dataset('data/test_data' , seg=SEG,
                            spatial_size=SPATIAL_SIZE , modality = 'SAME')

#give_dataset_insight(test_dataset)
## Making the Dataloader ##
test_dl = make_dataloader(test_dataset , 1)

total_dice = []

loop = tqdm(test_dl)
for idx , batch in enumerate(loop):
    mov = batch['fix'].to(device)
    fix = batch['mov'].to(device)

    if SEG:
        mov_seg = batch['mov_seg'].to(device)
        fix_seg = batch['fix_seg'].to(device)
        flow_field, pred_moving_vol, pred_mov_seg = vxm(fix, mov, fix_seg)
        pred_seg = torch.floor(pred_mov_seg)
        old_min, old_max = pred_seg.min(), pred_seg.max()
        new_min, new_max = 0., 5.
        pred_seg = (pred_seg - old_min)/(old_max - old_min) * (new_max - new_min) + new_min
        pred_seg = torch.round(torch.clip(pred_seg , min=0.0 , max=5.0))
        visualize_seg_slice(pred_seg[:1 , : , : , : , :],
                            mov_seg[:1 , : , : , : , :],
                            slice_idx=idx ,
                            save=True)
        dice_score = 1 - segmentation_loss()(pred_seg, mov_seg)
        total_dice.append(dice_score.item())
        loop.set_postfix(dice_score=dice_score.item())

    else:
        flow_field_1, pred_fixed_vol , flow_field_2 , pred_moving_vol = vxm(fix, mov)
        visualize_seg_slice(pred_moving_vol[:1, :, :, :, :],
                        mov[:1, :, :, :, :],
                        slice_idx=idx,
                        save=True)
        dice_score = 1 - segmentation_loss()(pred_moving_vol, mov)
        total_dice.append(dice_score.item())
        loop.set_postfix(dice_score=dice_score.item())


# if SEG == True:
f = open('dice.txt','w')
f.write(str(sum(total_dice) / len(total_dice)))
f.close()







