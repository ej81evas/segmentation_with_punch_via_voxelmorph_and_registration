import torch
import monai
import numpy as np

from glob import glob
from tqdm import tqdm

from monai.data import CacheDataset, DataLoader
from monai.transforms import Compose, AddChanneld, LoadImaged, Resized, ToTensord, ScaleIntensityRanged, Spacingd, RandSmoothDeformd

## Setting an utility function to make the dataset ##

def make_dataset(root_dir , intensity_mov = (-200 , 200) ,
                 intensity_fix = (0.0 , 0.13983),
                 spatial_size = (128 , 128 , 64) , seg = False, modality = 'SAME'):
    '''
    Returns the dataset instance by taking in the location of the root file location, root_dir, which contains
    the fixed and moving volumes subdirectories. (Optional can have the segmentation subdirectories too).
    :parameter
    root_dir : (str) The main directory containing the data.
    intensity_mov : (tup) Defines the lower bound and upper bound of the moving vol intensity. (Defaults: (-200,200))
    intensity_fix : (tup) Defines the lower bound and upper bound of the fix vol intensity. (0.0 , 0.13983))
    seg : (bool) Sets to True if one wants to load the segmentation volumes too. (Defaults: False).
    spatial_size : (tupe) Defines the dimension of the volume. (Defaults: (64,64,64))
    modality: (str) Defines if registration is done between same or different modalities. 
    options--> 'SAME' or 'DIFF'. (Defaults: 'SAME')
    '''

    ## Getting the filenames for the fixed and moving volumes ##

    # Loading the moving volume files #
    in_dim = str(spatial_size[0])
    files_mov = sorted(glob(root_dir + '/mov' + '/*.nii.gz'))

    # Loading the fixed volume file #
    # Since we have only one file we are going to repeat it for the length of train_mov so #
    # we get same number of filenames. It will help in dataloading #

    files_fix = sorted(glob(root_dir + '/fix' + in_dim + '/*.nii.gz')) * len(files_mov)

    ## Setting fixed files when modality is SAME ##
    if modality == 'SAME':
        files_fix = sorted(glob(root_dir + '/mov' + '/*.nii.gz'))
    assert (len(files_mov) == len(files_fix)), 'There is a mismatch among the number of moving and fixed volumes!'


    ## Setting transforms and loading dataset when seg is true and modality is same ##
    if seg and modality == 'SAME':
        files_mov_seg = sorted(glob(root_dir + '/mov_seg' + '/*.nii.gz'))
        files_fix_seg = files_mov_seg.copy()

        assert (len(files_mov) == len(files_mov_seg)), \
            'There is a mismatch among the number of volumes and segmentations!'
        data = [
            {'fix': fix_file,
             'mov': mov_file,
             'fix_seg': fix_seg_file,
             'mov_seg': mov_seg_file}
            for fix_file, mov_file, fix_seg_file, mov_seg_file in
            zip(files_fix, files_mov, files_fix_seg, files_mov_seg)
        ]

        ## Defining the Transformations ##

        trans = Compose([
            LoadImaged(keys=['fix', 'mov']),
            LoadImaged(keys=['fix_seg' , 'mov_seg'], dtype=np.int64),
            AddChanneld(keys=['fix', 'mov' , 'fix_seg' , 'mov_seg']),
            Spacingd(keys=['fix', 'mov'], pixdim=[1.5, 1.5, 1], mode=['bilinear', 'bilinear']),
            Resized(keys=['fix', 'mov' , 'fix_seg' , 'mov_seg'], spatial_size=spatial_size),
            ScaleIntensityRanged(keys=['mov'], a_min=intensity_mov[0], a_max=intensity_mov[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            ScaleIntensityRanged(keys=['fix_seg'], a_min=-1000., a_max=2909.,
                                 b_min=0.0, b_max=5.0, clip=True),
            ScaleIntensityRanged(keys=['fix'], a_min=intensity_fix[0], a_max=intensity_fix[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            RandSmoothDeformd(keys=['fix' , 'fix_seg'], spatial_size=spatial_size, rand_size=spatial_size),
            ToTensord(keys=['fix', 'mov' , 'fix_seg' , 'mov_seg'], dtype=torch.float32)
        ])
    
    ## Setting transforms and loading dataset when seg is False and modality is same ##
    elif seg is False and modality == 'SAME':
        data = [{'fix': fix_file, 'mov': mov_file} for fix_file, mov_file in zip(files_fix, files_mov)]

        ## Defining the Transformations ##

        trans = Compose([
            LoadImaged(keys=['fix', 'mov']),
            AddChanneld(keys=['fix', 'mov']),
            Spacingd(keys=['fix', 'mov'], pixdim=[1.5, 1.5, 1], mode=['bilinear', 'bilinear']),
            Resized(keys=['fix', 'mov'], spatial_size=spatial_size),
            ScaleIntensityRanged(keys=['mov'], a_min=intensity_mov[0], a_max=intensity_mov[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            ScaleIntensityRanged(keys=['fix'], a_min=intensity_fix[0], a_max=intensity_fix[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            RandSmoothDeformd(keys=['fix'], spatial_size = spatial_size , rand_size = spatial_size),
            ToTensord(keys=['fix', 'mov'], dtype=torch.float32)
        ])
    
    ## Setting transforms and loading dataset when seg is False and modality is different ##

    elif seg is False and modality == 'DIFF':
        data = [{'fix': fix_file, 'mov': mov_file} for fix_file, mov_file in zip(files_fix, files_mov)]

        ## Defining the Transformations ##

        trans = Compose([
            LoadImaged(keys=['fix', 'mov']),
            AddChanneld(keys=['fix', 'mov']),
            Spacingd(keys=['fix', 'mov'], pixdim=[1.5, 1.5, 1], mode=['bilinear', 'bilinear']),
            Resized(keys=['fix', 'mov'], spatial_size=spatial_size),
            ScaleIntensityRanged(keys=['mov'], a_min=intensity_mov[0], a_max=intensity_mov[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            ScaleIntensityRanged(keys=['fix'], a_min=intensity_fix[0], a_max=intensity_fix[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            ToTensord(keys=['fix', 'mov'], dtype=torch.float32)
        ])

    ## Setting transforms and loading dataset when seg is true and modality is DIFF ##
    else:
        files_mov_seg = sorted(glob(root_dir + '/mov_seg' + '/*.nii.gz'))
        files_fix_seg = sorted(glob(root_dir + '/fix' + in_dim + '_seg' + '/*.nii.gz')) * len(files_mov)

        assert (len(files_mov) == len(files_mov_seg)), \
            'There is a mismatch among the number of volumes and segmentations!'
        data = [
            {'fix': fix_file,
             'mov': mov_file,
             'fix_seg': fix_seg_file,
             'mov_seg': mov_seg_file}
            for fix_file, mov_file, fix_seg_file, mov_seg_file in
            zip(files_fix, files_mov, files_fix_seg, files_mov_seg)
        ]

        ## Defining the Transformations ##

        trans = Compose([
            LoadImaged(keys=['fix', 'mov']),
            LoadImaged(keys=['fix_seg', 'mov_seg'], dtype=np.int64),
            AddChanneld(keys=['fix', 'mov', 'fix_seg', 'mov_seg']),
            Resized(keys=['fix', 'mov', 'fix_seg', 'mov_seg'], spatial_size=spatial_size),
            ScaleIntensityRanged(keys=['mov'], a_min=intensity_mov[0], a_max=intensity_mov[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            ScaleIntensityRanged(keys=['fix_seg'], a_min=-1000., a_max=2909.,
                                 b_min=0.0, b_max=5.0, clip=True),
            ScaleIntensityRanged(keys=['fix'], a_min=intensity_fix[0], a_max=intensity_fix[1],
                                 b_min=0.0, b_max=1.0, clip=True),
            ToTensord(keys=['fix', 'mov', 'fix_seg', 'mov_seg'], dtype=torch.float32)
        ])
    ## Creating the monai cachedataset ##

    dataset = CacheDataset(data=data,
                           transform=trans,
                           progress=True)

    return dataset



def make_dataloader(dataset , batch_size = 2 , shuffle = True):
    '''
    Creates the dataloader using monai.
    :param dataset: The predefined monai dataset.
    :param batch_size: Number of datapoint to be taken as a minibatch.
    :param shuffle: Sets to True if the dataset is to be shuffled or not before selecting minibatch.
    :return: Monai Dataloader.
    '''

    ## Making our dataloader ##

    dl = DataLoader(dataset,
                    batch_size=batch_size,
                    shuffle=shuffle)

    return dl

def give_dataset_insight(dataset):

    ## Displaying some insight about a data point ##

    print('Shape of the dataset is :', len(dataset))
    print('Shape of the fixed volume is :', dataset[0]['fix'].shape)
    print('Shape of the moving volume is :', dataset[0]['mov'].shape)
    print('Maximum value of the fixed volume is :', torch.max(dataset[0]['fix']))
    print('Minimum value of the fixed volume is :', torch.min(dataset[0]['fix']))
    print('Maximum value of the moving volume is :', torch.max(dataset[0]['mov']))
    print('Minimum value of the moving volume is :', torch.min(dataset[0]['mov']))

    if dataset[0].get('fix_seg') is not None:
        print('Shape of the fixed vol segmentation map is :', dataset[0]['fix_seg'].shape)
        print('Shape of the moving vol segmentation map is :', dataset[0]['mov_seg'].shape)
        print('Segmentation classes of fixed vol are :', torch.unique(dataset[0]['fix_seg']))
        print('Segmentation classes of moving vol are :', torch.unique(dataset[0]['mov_seg']))


def give_dl_insight(dl):
    ## Testing out the dataloader ##

    print('Dataloader length is :' , len(dl))

    for batch in dl:
        print('Moving Volume shape :', batch['mov'].shape)
        print('Fixed Volume shape :', batch['fix'].shape)
        if batch.get('fix_seg') is not None:
            print('Moving Volume Segmentation shape :', batch['mov_seg'].shape)
            print('Fixed Volume Segmentation shape :', batch['fix_seg'].shape)

        break