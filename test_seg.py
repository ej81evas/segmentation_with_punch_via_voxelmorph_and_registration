import torch
import numpy as np
from glob import glob
from data_utilities.dataset import make_dataset, make_dataloader , give_dataset_insight
from utility.default_device import get_device
from tqdm import tqdm
from losses.loss import smoothing_loss, segmentation_loss, similarity_loss
from utility.segment_slice_vis import visualize_seg_slice
#from utility.flow_field_vis import visualize_flow_field
from models.voxelmorph import VoxelMorph64

## Setting the default device ##
device = get_device()

SEG = True
SPATIAL_SIZE = (64 , 64 , 64)
MODALITY = 'SAME'

## Setting the model path ##
model_path = sorted(glob('model_dir' + '/*'))[-1]

## Loading the model ##
vxm = VoxelMorph64(SPATIAL_SIZE)
vxm.load_state_dict(torch.load(model_path))
vxm = vxm.to(device).eval()
print('Model loaded Successfully!')

## Making test dataset ##
test_dataset = make_dataset('data/test_data' , seg=SEG,
                            spatial_size=SPATIAL_SIZE ,
                            modality = MODALITY)

#give_dataset_insight(test_dataset)
## Making the Dataloader ##
test_dl = make_dataloader(test_dataset , 1)

total_dice = []

loop = tqdm(test_dl)
for idx , batch in enumerate(loop):
    fix = batch['fix'].to(device)
    mov = batch['mov'].to(device)

    if SEG:
        mov_seg = batch['mov_seg'].to(device)
        fix_seg = batch['fix_seg'].to(device)
        flow_field, warped_fix, warped_fix_seg = vxm(fix, mov, mov_seg)
        pred_seg = torch.floor(warped_fix_seg)

        #print(pred_seg)
        visualize_seg_slice(mov_seg[:1 , : , : , : , :] , 
                            mov_seg[:1 , : , : , : , :],
                            slice_idx=idx ,
                            save=True)

        dice_score = 1 - segmentation_loss()(pred_seg, fix_seg)
        total_dice.append(dice_score.item())
        loop.set_postfix(dice_score=dice_score.item())

    else:
        flow_field, warped_fix = vxm(fix, mov)
        visualize_seg_slice(fix[:1, :, :, :, :],
                            warped_fix[:1 , : , : , : , :],
                            slice_idx=idx,
                            save=True)
        dice_score = 1 - segmentation_loss()(warped_fix, fix)
        total_dice.append(dice_score.item())
        loop.set_postfix(dice_score=dice_score.item())

    #break


f = open('dice.txt','w')
f.write(str(sum(total_dice) / len(total_dice)))
f.close()
#print('The mean Dice score is :' , sum(total_dice) / len(total_dice))






