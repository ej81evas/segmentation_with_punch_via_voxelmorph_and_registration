import torch
import numpy as np
from glob import glob
from data_utilities.dataset import make_dataset, make_dataloader , give_dataset_insight
from utility.default_device import get_device
from tqdm import tqdm
from losses.loss import smoothing_loss, segmentation_loss, similarity_loss
from utility.seg_slice_vis import visualize_seg_slice
#from utility.flow_field_vis import visualize_flow_field
from models.voxelmorph import VoxelMorph64

## Setting the default device ##
device = get_device()

SEG = False
SPATIAL_SIZE = (64 , 64 , 64)

## Setting the model path ##
model_path = sorted(glob('model_dir' + '/*'))[-1]

## Loading the model ##
vxm = VoxelMorph64(SPATIAL_SIZE)
vxm.load_state_dict(torch.load(model_path))
vxm = vxm.to(device).eval()
print('Model loaded Successfully!')

## Making test dataset ##
test_dataset = make_dataset('data/test_data' , seg=SEG,
                            spatial_size=SPATIAL_SIZE , modality = 'DIFF')

#give_dataset_insight(test_dataset)
## Making the Dataloader ##
test_dl = make_dataloader(test_dataset , 1)

total_dice = []

loop = tqdm(test_dl)
for idx , batch in enumerate(loop):
    fix = batch['fix'].to(device)
    mov = batch['mov'].to(device)

    #visualize_seg_slice(fix, mov,slice_idx=idx,save=True)
    # if SEG:
    #     mov_seg = batch['mov_seg'].to(device)
    #     fix_seg = batch['fix_seg'].to(device)
    #     flow_field, pred_fixed, pred_fixed_seg = vxm(fix, mov, mov_seg)
    #     pred_seg = torch.round(pred_fixed_seg)
    #
    #     visualize_seg_slice(torch.clip(pred_seg[:1 , : , : , : , :] , min = 0.0 , max = 5.0),
    #                         torch.clip(mov_seg[:1 , : , : , : , :] , min = 0.0 , max = 5.0),
    #                         slice_idx=idx ,
    #                         save=True)

    # else:
    flow_field, pred_fixed = vxm(fix, mov)
    visualize_seg_slice(pred_fixed[:1, :, :, :, :],
                        fix[:1, :, :, :, :],
                        slice_idx=idx,
                        save=True)

    #visualize_flow_field(flow_field[:1 , : , : , : , :] , idx = idx, save = True)

    # if SEG == True:
    dice_score = 1 - segmentation_loss()(pred_fixed, fix)
    total_dice.append(dice_score.item())
    loop.set_postfix(dice_score = dice_score.item())

# if SEG == True:
f = open('dice.txt','w')
f.write(str(sum(total_dice) / len(total_dice)))
f.close()
#print('The mean Dice score is :' , sum(total_dice) / len(total_dice))






