import torch
from torch import nn
import torch.nn.functional as F
import monai
from monai.losses import DiceLoss, LocalNormalizedCrossCorrelationLoss

def smoothing_loss(y_pred , penalty='l1'):
    """
    Calculates the smoothing loss function of the flow field.
    """

    ## Code of the smoothing loss function is reused from here ##
    ## (https://github.com/voxelmorph/voxelmorph/blob/dev/voxelmorph/torch/losses.py) ##

    dy = torch.abs(y_pred[:, :, 1:, :, :] - y_pred[:, :, :-1, :, :])
    dx = torch.abs(y_pred[:, :, :, 1:, :] - y_pred[:, :, :, :-1, :])
    dz = torch.abs(y_pred[:, :, :, :, 1:] - y_pred[:, :, :, :, :-1])

    if penalty == 'l2':
        dy = dy * dy
        dx = dx * dx
        dz = dz * dz

    d = torch.mean(dx) + torch.mean(dy) + torch.mean(dz)
    grad = d / 3.0

    return grad


## Instantiating the similarity loss ##

def similarity_loss(kernel_size):
    """
    Calculates the similarity between the warped predicted volume and the
    fixed volume.
    """

    sim_loss = LocalNormalizedCrossCorrelationLoss(kernel_size = kernel_size)

    return sim_loss

## Instantiating the Dice Loss ##

def segmentation_loss():
    """
    Calculates the similarity between the warped predicted volume segmentation
    and the fixed volume segmentation.
    """
    seg_loss = DiceLoss(squared_pred = True)

    return seg_loss


## Defining contrastive loss ##

class ContrastiveLoss(nn.Module):

    def __init__(self, margin):
        super().__init__()

        self.margin = margin

    def forward(self, map_1, map_2):
        euclidean_distance = F.pairwise_distance(map_1, map_2)

        loss_contrastive = torch.mean(torch.pow(torch.clamp(self.margin - euclidean_distance, min=0.0), 2))

        return loss_contrastive