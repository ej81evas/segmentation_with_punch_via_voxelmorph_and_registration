import torch

## Setting the default device ##

def get_device():
    '''
    Sets the default device.
    '''
    if torch.cuda.is_available():
        return torch.device('cuda')
    return torch.device('cpu')