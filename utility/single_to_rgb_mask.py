import numpy as np

## Utility to convert single channel mask to RGB mask ##
import torch


def single_to_rgb(pred_tensor , mapping = None):
    '''
    Changes the single channel segmentation map to rgb map.
    :param mapping: Color mapping schemes.
    :param pred_tensor: A pytorch tensor.
    :return: An rgb numpy array of segmentation map.
    '''

    if mapping is None:
        mapping = {
            0: [0., 0., 0.],
            1: [204., 204., 255.],
            2: [204., 255., 255.],
            3: [255., 255., 204.],
            4: [255., 204., 255.],
            5: [255., 255., 229.]
        }

    #print(torch.unique(pred_tensor))

    pred_array = pred_tensor.cpu().detach().numpy()

    if len(pred_tensor.shape) == 3:
        pred_array = pred_array.squeeze(0)

    elif len(pred_tensor.shape) == 4:
        pred_array = pred_array.squeeze(0).squeeze(0)

    segmentation_map = np.zeros((pred_array.shape[0],
                                 pred_array.shape[1],
                                 3))

    for i in range(pred_array.shape[0]):
        for j in range(pred_array.shape[1]):
            map_val = pred_array[i, j]
            segmentation_map[i, j] = mapping[map_val]

    segmentation_map = segmentation_map / 255.
    return segmentation_map