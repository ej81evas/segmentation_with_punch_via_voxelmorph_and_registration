import numpy as np
from utility.single_to_rgb_mask import single_to_rgb
import matplotlib.pyplot as plt

## lets visualize some slices of the original and predicted segmentation ##

def visualize_seg_slice(pred, target , slice_idx , save = False):
    idx = 32

    pred = pred.squeeze(0)

    target = target.squeeze(0)

    pred_slice = pred[:, :, :, idx]

    target_slice = target[:, :, :, idx]

    pred_slice = single_to_rgb(pred_slice.permute(0, 2, 1))

    target_slice = single_to_rgb(target_slice.permute(0, 2, 1))

    # plt.title('Idx is :' + str(idx))

    plt.subplot(2, 1, 1)

    plt.imshow(pred_slice)

    plt.subplot(2, 1, 2)

    plt.imshow(target_slice)

    if save == True:
        plt.savefig('results/seg_slice/seg_{:02d}.png'.format(slice_idx), dpi=300)

    #plt.show()