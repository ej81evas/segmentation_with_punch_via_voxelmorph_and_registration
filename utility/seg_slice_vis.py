import numpy as np
from utility.single_to_rgb_mask import single_to_rgb
import matplotlib.pyplot as plt

## lets visualize some slices of the original and predicted segmentation ##

def visualize_seg_slice(pred, target , slice_idx , save = False , seg = False):
    idx = 32

    pred = pred.squeeze(0)

    target = target.squeeze(0)

    pred_slice = pred[:, :, :, idx]

    target_slice = target[:, :, :, idx]

    if seg == True:
        pred_slice = single_to_rgb(pred_slice.permute(0, 2, 1))

        target_slice = single_to_rgb(target_slice.permute(0, 2, 1))

    else:
        pred_slice = pred_slice.permute(0, 2, 1).squeeze(0).cpu().detach().numpy()

        target_slice = target_slice.permute(0, 2, 1).squeeze(0).cpu().detach().numpy()


    # plt.title('Idx is :' + str(idx))

    plt.subplot(2, 1, 1)

    plt.xticks([])
    plt.yticks([])

    plt.imshow(pred_slice , cmap = 'gray')

    plt.subplot(2, 1, 2)

    plt.xticks([])
    plt.yticks([])

    plt.imshow(target_slice , cmap = 'gray')

    if save == True:
        plt.savefig('results/seg_slice/seg_{:02d}.png'.format(slice_idx), dpi=300)

    #plt.show()