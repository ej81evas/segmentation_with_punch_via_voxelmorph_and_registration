import neurite as ne
import matplotlib.pyplot as plt

## Visualization of flow fields ##

def visualize_flow_field(flow_field , idx , save = False):
    if len(flow_field.shape) == 5:
        disp_flow = flow_field[0, 0, :, :, :]

    if len(flow_field.shape) == 4:
        disp_flow = flow_field[0, :, :, :]

    disp_flow_slices = []

    for i in range(0, disp_flow.shape[0], 2):
        disp_flow_slices.append(disp_flow[i: i + 2].permute(1, 2, 0).detach().cpu().numpy())

    if save == True:
        ne.plot.flow(disp_flow_slices, width=5, grid=True, scale=0.5 , show=False)
        plt.savefig('results/flow_field/flow__{:02d}.png'.format(idx), dpi=300)
        plt.show()

    else:
        ne.plot.flow(disp_flow_slices, width=5, grid=True, scale=0.5)