from data_utilities.dataset import make_dataset, make_dataloader, give_dataset_insight, give_dl_insight
from models.voxelmorph import VoxelMorph256
from losses.loss import smoothing_loss, segmentation_loss, similarity_loss
from utility.default_device import get_device

import torch
import monai
import numpy as np
from tqdm import tqdm
import pandas as pd

## Setting the hyperparameters ##
#SEG_REG = 10
SMOOTH_REG = 0.01
NUM_EPOCHS = 500
SEG = False
INP = 256

## Setting the default device ##
device = get_device()

## Setting the Dataset and the Dataloader ##
train_dataset = make_dataset('data/train_data' , spatial_size = (256 , 256 , 64) , seg=SEG , modality = 'SAME')
train_dl = make_dataloader(train_dataset)

## Setting the VoxelMorph ##
vxm = VoxelMorph256(input_shape=(256 , 256 , 64)).to(device)

## Setting the similarity and segmentation loss functions ##
seg_loss = segmentation_loss()
sim_loss = similarity_loss(kernel_size=3)

## Setting the optimizer ##
optim = torch.optim.Adam(vxm.parameters() , lr = 2e-3 , amsgrad = True)

total_loss_list = []
seg_loss_list = []
sim_loss_list = []
smooth_loss_list = []

for epoch in range(NUM_EPOCHS):

    loop = tqdm(train_dl)

    batch_total_loss_list = []
    batch_seg_loss_list = []
    batch_sim_loss_list = []
    batch_smooth_loss_list = []

    for batch in loop:

        fix = batch['fix'].to(device)
        mov = batch['mov'].to(device)

        optim.zero_grad()

        if SEG == False:
            flow_field, pred_fixed = vxm(fix, mov)

        else:
            fix_seg = batch['fix_seg'].to(device)
            flow_field, pred_fixed , pred_fixed_seg = vxm(fix, mov , fix_seg)


        sm_loss = smoothing_loss(flow_field, penalty='l2')

        si_loss = -sim_loss(pred_fixed, fix)

        if SEG == False:
            total_loss =  si_loss + SMOOTH_REG * sm_loss

        else:
            mov_seg = batch['mov_seg'].to(device)
            sg_loss = seg_loss(pred_fixed_seg , mov_seg)
            total_loss = si_loss + sg_loss + SMOOTH_REG * sm_loss

        total_loss.backward()

        optim.step()

        batch_total_loss_list.append(total_loss.item())
        batch_sim_loss_list.append(si_loss.item())
        batch_smooth_loss_list.append(sm_loss.item())

        if SEG == True:
            batch_seg_loss_list.append(sg_loss.item())


        loop.set_description('Epoch : {} / {}'.format(epoch + 1, NUM_EPOCHS))

        if SEG == False:
            loop.set_postfix(loss=total_loss.item(), smooth_loss=sm_loss.item(), similarity_loss=si_loss.item())
        else:
            loop.set_postfix(loss=total_loss.item(), smooth_loss=sm_loss.item(),
                             similarity_loss=si_loss.item(), segmentation_loss=sg_loss.item())

    total_loss_list.append(sum(batch_total_loss_list) / len(batch_total_loss_list))
    if SEG == True:
        seg_loss_list.append(sum(batch_seg_loss_list) / len(batch_seg_loss_list))
    sim_loss_list.append(sum(batch_sim_loss_list) / len(batch_sim_loss_list))
    smooth_loss_list.append(sum(batch_smooth_loss_list) / len(batch_smooth_loss_list))

    if (epoch + 1) % 10 == 0:
        torch.save(vxm.state_dict() , 'model_dir/model_{:03d}.pt'.format(epoch + 1))

if SEG == True:
    loss_df = pd.DataFrame(list(zip(total_loss_list , seg_loss_list,
                                    sim_loss_list , smooth_loss_list)) ,
                           columns = ['Total Loss' , 'Segmentation Loss' ,
                                      'Similarity Loss' , 'Smoothing Loss'])
else:
    loss_df = pd.DataFrame(list(zip(total_loss_list,
                                    sim_loss_list, smooth_loss_list)),
                           columns=['Total Loss',
                                    'Similarity Loss', 'Smoothing Loss'])

loss_df.to_csv('loss_' + str(INP) +'.csv' , header=True , index=False)